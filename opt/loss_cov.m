function [L, Lx, Lxx, Lu, Luu] = loss_cov(k, x, u, P, S)
% cost (just standard quadratic cost)

L = S.h/2*((x-S.xd)'*S.Q*(x-S.xd) + (u-S.ud)'*S.R*(u-S.ud) + S.T*det(P(1:2,1:2)));
Lx = S.h*S.Q*(x-S.xd);
Lxx = S.h*S.Q;
Lu = S.h*S.R*(u-S.ud);
Luu = S.h*S.R;
end
