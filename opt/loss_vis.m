function [L, Lx, Lxx, Lu, Luu] = loss_vis(k, x, u, S)
% cost (just standard quadratic cost)

L = S.h/2*((x-S.xd)'*S.Q*(x-S.xd) + (u-S.ud)'*S.R*(u-S.ud) + S.S*vismetric(x, S));
Lx = S.h*S.Q*(x-S.xd);
Lxx = S.h*S.Q;
Lu = S.h*S.R*(u-S.ud);
Luu = S.h*S.R;
end

function vis = vismetric(x, S)
p = x(1:2);
vis = 0;
for i=1:size(S.pbs, 2)
  pb = S.pbs(:, i); %i-th beacon
  d = pb - p;
  r = norm(d);
  
  if (r > S.limit)
      vis = vis + S.limit;
  else
      vis = vis + r;
  end  
end
end