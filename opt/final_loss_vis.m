function [L, Lx, Lxx] = final_loss_vis(x, S)
% cost (just standard quadratic cost)

L = (x-S.xd)'*S.Qf*(x-S.xd)/2 + S.S*vismetric(x, S);
Lx = S.Qf*(x-S.xd);
Lxx = S.Qf;
end

function vis = vismetric(x, S)
p = x(1:2);
vis = 0;
for i=1:size(S.pbs, 2)
  pb = S.pbs(:, i); %i-th beacon
  d = pb - p;
  r = norm(d);
  
  if (r > S.limit)
      vis = vis + S.limit;
  else
      vis = vis + r;
  end  
end
end