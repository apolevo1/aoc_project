function [L, Lx, Lxx] = final_loss_cov(x, P, S)
% cost (just standard quadratic cost)

L = ((x-S.xd)'*S.Qf*(x-S.xd) + S.T*det(P(1:2,1:2)))/2;
Lx = S.Qf*(x-S.xd);
Lxx = S.Qf;
end
