function [L, Lx, Lxx, Lu, Luu] = loss(k, x, u, S)
% cost (just standard quadratic cost)

L = S.h/2*((x-S.xd)'*S.Q*(x-S.xd) + (u-S.ud)'*S.R*(u-S.ud));
Lx = S.h*S.Q*(x-S.xd);
Lxx = S.h*S.Q;
Lu = S.h*S.R*(u-S.ud);
Luu = S.h*S.R;
end
