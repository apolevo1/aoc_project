function z = fix_meas(z, S)
for i=1:length(z)/2
  z(2*i-1) = fangle(z(2*i-1));
end
