%% State Estimation
% Perform state estimation on unicycle dynamical system

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('dysys');
addpath('est');
addpath('utility');
addpath('meas');

% initialize rng
rng(10212)

%% Set up beacons

% two beacons at (-2,2) and (2,2) : system is observable (two or more)
S.pbs = [-2, 2;
         2, 2];    % beacon positions


%% Set up measurement model

S.H = @br_h;   % bearing-reange sensing
S.E = [.04; .004];

%% Set up dynamical system

S.n = 3;       % state dimension
S.f = @uni_f;  % mobile-robot dynamics

%% Set up timing

dt = .1;
N = 50;
T = dt*N;
S.dt = dt;

%% Initialize everything

% noise models
S.O = .3*dt*diag([.1 .1 .01]);

% true state
xt = [0; 0; pi/4]; % true state

% initial mean and covariance
P = .2*diag([1 1 .1]);
xe = xt + sqrt(P)*randn(S.n, 1);

% matrices to keep track of everything
xts = zeros(S.n, N+1); % true states
xes = zeros(S.n, N+1);  % estimated states
Ps = zeros(S.n, S.n, N+1); % estimated covariances
ts = zeros(N+1,1); % times
ds = zeros(S.n, N+1);  % errors

% first values in matrices
xts(:, 1) = xt;
xes(:, 1) = xe;
Ps(:, :, 1) = P;
ts(1) = 0;
ds(:,1) = xe - xt;

%% Simulate

for k=1:N
  
  % Set control
  u = dt*[2; 1];
  
  % Propogate true state and time
  xt = S.f(xts(:,k), u, S) + sqrt(S.O)*randn(3,1);
  t = k*dt;

  % Propogate state estimation 
  [xe,P] = ekf_predict(xe, P, u, S);                  % predict
  [y, idx] = S.H(xt, S);
  E = diag(repmat(S.E, length(y)/2, 1));
  z = y + sqrt(E)*randn(length(y),1);            % generate measurement  
  [xe,P] = ekf_correct(xe, P, z, idx, S);                  % correct
  
  % Save everything
  xts(:,k+1) = xt;
  ts(k+1) = t;
  xes(:,k+1) = xe;
  Ps(:,:,k+1) = P;
  ds(:,k+1) = xe - xts(:,k+1);  % actual estimate error
  ds(:,k+1) = fix_state(ds(:,k+1));
    
end

%% Plot results

% Show trajectory and state estimation
fig1=figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
xlabel('xe');
ylabel('y');
axis equal;
axis xy;
axis([-4 3 -4 4]);
plot(S.pbs(1,:), S.pbs(2,:), '*r');                   % Plot beacons

for k=1:N
  
  plot(xts(1,1:k), xts(2,1:k), '--g','LineWidth',3)     % Plot true states
  plot(xes(1,1:k), xes(2,1:k), '-b','LineWidth',3)        % Plot est states
  plotcov2(xes(1:2,k), 1.96^2*Ps(1:2,1:2,k));            % Plot covariances
  drawnow;
  
end

% Clean up plot
legend('true', 'estimated');

% Show errors and covariances
fig2=figure;

% Plot errors
subplot(2,1,1)
plot(ds(:,1:k)')
xlabel('k')
ylabel('meters or radians')
legend('e_x','e_y','e_\theta')
  
% Plot covariances
subplot(2,1,2)
plot(ts(1:N), reshape(sqrt(Ps(1,1,1:N)),N,1), ...
     ts(1:N), reshape(sqrt(Ps(2,2,1:N)),N,1), ...
     ts(1:N), reshape(sqrt(Ps(3,3,1:N)),N,1));
legend('\sigma_x','\sigma_y','\sigma_\theta')
xlabel('t')
ylabel('meters or radians')  