function [y, idx, varargout] = br_h_limit(x, S)

p = x(1:2);

y = [];
H = [];
idx = [];
for i=1:size(S.pbs, 2)
  pb = S.pbs(:, i); %i-th beacon
  d = pb - p;
  r = norm(d);
  
  if (r > S.limit)
      continue;
  end
  
  th = fangle(atan2(d(2), d(1)) - x(3));
  y = [y; th; r];
  idx = [idx; i];
  
  if nargout > 1
    % H-matrix
    H = [H;
         d(2)/r^2, -d(1)/r^2, -1;
         -d'/r, 0];
  end
end

if nargout > 1  
  varargout{1} = H;
end
