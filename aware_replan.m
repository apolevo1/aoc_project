%% Aware Replan
% Perform perception aware replanning control

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('dysys');
addpath('opt');
addpath('est');
addpath('utility');
addpath('meas');

% initialize rng
rng('shuffle')

%% Choose metric

% metric = 'normal';
% metric = 'visibility';
metric = 'covariance';

%% Timing

dt = .1;
N = 50;
T = dt*N;   
S.dt = dt;
S.h = dt;
S.tf = T;
S.N = N;

%% Beacons

% beacon positions
S.pbs = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, ...
              1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, ...
                   1.5, 2.0, 2.5, 3.0, 3.5, 4.0, ...
                        2.0, 2.5, 3.0, 3.5, 4.0, ...
                             2.5, 3.0, 3.5, 4.0, ...
                                  3.0, 3.5, 4.0, ...
                                       3.5, 4.0, ...
                                            4.0; ...
         -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, ...
              0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ...
                   0.5, 0.5, 0.5, 0.5, 0.5, 0.5, ...
                        1.0, 1.0, 1.0, 1.0, 1.0, ...
                             1.5, 1.5, 1.5, 1.5, ...
                                  2.0, 2.0, 2.0, ...
                                       2.5, 2.5, ...
                                            3.0];
     
%% Dynamical Model

S.n = 3;                                                                    % state dimension
S.f = @uni_f;                                                               % mobile-robot dynamics

%% Measurement Model

S.limit = 1;
S.H = @br_h_limit;                                                                % bearing-reange sensing

%% Noise Model

S.O = diag([.001 .001 .0001]);                                              % dynamics noise
S.E = [.01; .001];

%% Planning Parameters

% cost function parameters
S.Q = diag([0.005, 0.005, 0]);
S.R = diag([1, 5]);
S.Qf = diag([5, 5, 0]);

% loss functions
if strcmp(metric, 'normal') 
    S.L = @loss;
    S.Lf = @final_loss;
elseif strcmp(metric, 'visibility')
    S.S = .1;
    S.L = @loss_vis;
    S.Lf = @final_loss_vis;
elseif strcmp(metric, 'covariance')
    S.T = 1e5;                      % 1e6 works
    S.Tf = 1e5;                     % 1e6 works
    S.L = @loss_cov;
    S.Lf = @final_loss_cov;
end

% horizon for replanning
horizon = 5;

%% Initialize Everything for Optimization

% initial covariance
P = diag([0.01 0.01 .001]);                                                 % initial covariance
S.P = P;

% initial state
x0 = [0; 0; pi/2];                                                             % initial planned state
xt = x0;                                                                    % initial true state
xe = xt + sqrt(P)*randn(S.n, 1);                                            % initial estimated state

% initialize matrices to track everything
us = zeros(2,S.N);                                                          % planned control
xs = traj(x0, us, S);                                                       % planned states
xts = zeros(S.n, N+1);                                                      % true states
xes = zeros(S.n, N+1);                                                      % estimated states
Ps = zeros(S.n, S.n, N+1);                                                  % estimated covariances
ts = zeros(N+1,1);                                                          % times
ds = zeros(S.n, N+1);                                                       % errors

% first values
xts(:, 1) = xt;
xes(:, 1) = xe;
Ps(:, :, 1) = P;
ts(1) = 0;
ds(:,1) = xe - xt;

% goals
S.xd = [3.25;3.75;0];                                                             % desired goal state
S.ud = [0;0];                                                               % desired control

%% Prepare Plots
figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
xlabel('p_x');
ylabel('p_y');
axis equal;
axis xy;
plot(S.pbs(1,:), S.pbs(2,:), '*r');                                         % Plot beacons

if strcmp(metric, 'normal') 
    title('Normal Metric')
elseif strcmp(metric, 'visibility')
    title('Visibility Metric')
elseif strcmp(metric, 'covariance')
    title('Covariance Metric')
end

%% Simulate

for k=1:N
    
  if mod(k, horizon) == 1
      
    if k ~= 1
        h.Color = [0,0,0,0.1];
    end
      
    u = us(1:2,k:S.N);                                                      % planned control    
    x = traj(xe, u, S);                                                     % planned states

    % plan trajectory
    if strcmp(metric, 'normal')
        [x, u, blah, ~,~] = dircol(x, u, S, @plottraj);                    
    elseif strcmp(metric, 'visibility')
        [x, u, blah, ~,~] = dircol_vis(x, u, S, @plottraj);                
    elseif strcmp(metric, 'covariance')
        [x, u, blah, ~,~] = dircol_cov(x, u, S, @plottraj);
    end
    
    us(:,k:N) = u;                                                          % save planned control
    xs(:,k:N+1) = x;                                                        % save planned states
    
    h = plot(xs(1,k:N+1), xs(2,k:N+1), 'Color', [0,0,0],'LineWidth',2);     % Plot planned states
    drawnow;                                                                % display plot
  end
    
  % Set control
  u = us(:,k);
  
  % Propogate true state and time
  xt = S.f(xts(:,k), u, S) + sqrt(S.O)*randn(3,1);
  t = k*dt;

  % Propogate state estimation 
  [xe,P] = ekf_predict(xe, P, u, S);                                        % predict
  [y, idx] = S.H(xt, S);
  E = diag(repmat(S.E, length(y)/2, 1));
  z = S.H(xt, S) + sqrt(E)*randn(length(y),1);                              % generate measurement  
  [xe,P] = ekf_correct(xe, P, z, idx, S);                                   % correct
    
  % Save everything
  xts(:,k+1) = xt;
  ts(k+1) = t;
  xes(:,k+1) = xe;
  Ps(:,:,k+1) = P;
  ds(:,k+1) = xe - xts(:,k+1);                                              % actual estimate error
  ds(:,k+1) = fix_state(ds(:,k+1));
    
  % Plot true and estimated states
  plot(xts(1,1:k), xts(2,1:k), '-g','LineWidth',2)                         % Plot true states
  plot(xes(1,1:k), xes(2,1:k), '-b','LineWidth',2)                          % Plot est states
  plotcov2(xes(1:2,k), 1.96^2*Ps(1:2,1:2,k));                               % Plot covariances
  plotcircle(xes(1,k),xes(2,k),S.limit);
  drawnow;
  
end

% Plot true and estimated states
plot(xts(1,1:k+1), xts(2,1:k+1), '-g','LineWidth',2)                         % Plot true states
plot(xes(1,1:k+1), xes(2,1:k+1), '-b','LineWidth',2)                          % Plot est states
plotcov2(xes(1:2,k+1), 1.96^2*Ps(1:2,1:2,k+1));                               % Plot covariances
plotcircle(xes(1,k+1),xes(2,k+1),S.limit);
drawnow;

%% Print results

err = xts(:,end)-S.xd;
disp(['Fin err: ', num2str(norm(err(1:2)))]);
disp(['Fin  det(P): ', num2str(det(Ps(1:2,1:2,end)))]);
disp(['Mean det(P): ', num2str(det(mean(Ps(1:2,1:2,:),3)))]);