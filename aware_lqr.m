%% Aware LQR
% Perform perception aware tvlqr control

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('dysys');
addpath('opt');
addpath('est');
addpath('utility');
addpath('meas');

% initialize rng
rng('shuffle')

%% Simulate Time

% simtime = 1;
simtime = 0;

%% Choose metric

% metric = 'normal';
metric = 'visibility';
% metric = 'covariance';

%% Timing

dt = .1;
N = 50;
T = dt*N;
S.dt = dt;
S.h = dt;
S.tf = T;
S.N = N;

%% Beacons

% beacon positions
S.pbs = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, ...
    1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, ...
    1.5, 2.0, 2.5, 3.0, 3.5, 4.0, ...
    2.0, 2.5, 3.0, 3.5, 4.0, ...
    2.5, 3.0, 3.5, 4.0, ...
    3.0, 3.5, 4.0, ...
    3.5, 4.0, ...
    4.0; ...
    -.5, -.5, -.5, -.5, -.5, -.5, -.5, -.5, ...
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ...
    0.5, 0.5, 0.5, 0.5, 0.5, 0.5, ...
    1.0, 1.0, 1.0, 1.0, 1.0, ...
    1.5, 1.5, 1.5, 1.5, ...
    2.0, 2.0, 2.0, ...
    2.5, 2.5, ...
    3.0];

%% Dynamical Model

S.n = 3;                                                                    % state dimension
S.f = @uni_f;                                                               % mobile-robot dynamics

%% Measurement Model

S.limit = 1;
S.H = @br_h_limit;                                                                % bearing-reange sensing

%% Noise Model

S.O = diag([.001 .001 .0001]);                                              % dynamics noise
S.E = [.01; .001];

%% LQR Parameters

Ql = 10*diag([10, 10, 10]);
Rl = 10*diag([1, 1]);

%% Planning Parameters

% cost function parameters
S.R = diag([1, 5]);
S.Qf = diag([5, 5, 0]);

% loss functions
if strcmp(metric, 'normal')
    S.Q = S.Qf / 500;
    S.L = @loss;
    S.Lf = @final_loss;
elseif strcmp(metric, 'visibility')
    S.Q = S.Qf / 500;
    S.S = 1e-1;
    S.L = @loss_vis;
    S.Lf = @final_loss_vis;
elseif strcmp(metric, 'covariance')
    S.Q = S.Qf / 500;
    S.Tf = 5e7;
    S.T = S.Tf / 500;
    S.L = @loss_cov;
    S.Lf = @final_loss_cov;
end

%% Initialize Everything for Optimization

% initial covariance
P = diag([0.01 0.01 .001]);                                                 % initial covariance
S.P = P;

% initial state
x0 = [0; 0; pi/2];                                                             % initial planned state
xt = x0;                                                                    % initial true state
xe = xt + sqrt(P)*randn(S.n, 1);                                            % initial estimated state

% initialize matrices to track everything
us = zeros(2,S.N);                                                          % planned control
xs = traj(x0, us, S);                                                       % planned states
xts = zeros(S.n, N+1);                                                      % true states
xes = zeros(S.n, N+1);                                                      % estimated states
Ps = zeros(S.n, S.n, N+1);                                                  % estimated covariances
ts = zeros(N+1,1);                                                          % times
ds = zeros(S.n, N+1);                                                       % errors

% first values
xts(:, 1) = xt;
xes(:, 1) = xe;
Ps(:, :, 1) = P;
ts(1) = 0;
ds(:,1) = xe - xt;

% goals
S.xd = [3.25;3.75;0];                                                             % desired goal state
S.ud = [0;0];                                                               % desired control

%% Prepare Plots
figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
xlabel('p_x');
ylabel('p_y');
axis equal;
axis xy;
plot(S.pbs(1,:), S.pbs(2,:), '*r');                                         % Plot beacons

if strcmp(metric, 'normal')
    title('Normal Metric')
elseif strcmp(metric, 'visibility')
    title('Visibility Metric')
elseif strcmp(metric, 'covariance')
    title('Covariance Metric')
end

%% Generate Optimal Trajectory

xs = traj(xe, us, S);                                                     % planned states

% plan trajectory
if strcmp(metric, 'normal')
    [xs, us, blah, ~,~] = dircol(xs, us, S, @plottraj);
elseif strcmp(metric, 'visibility')
    [xs, us, blah, ~,~] = dircol_vis(xs, us, S, @plottraj);
elseif strcmp(metric, 'covariance')
    [xs, us, blah, ~,~] = dircol_cov(xs, us, S, @plottraj);
end

h = plot(xs(1,:), xs(2,:), 'Color', [0,0,0],'LineWidth',2);     % Plot planned states
drawnow;

%% Simulate

if (simtime)
    w = waitforbuttonpress;
    w = waitforbuttonpress;
    w = waitforbuttonpress;
end
tic;

for k=1:N
    
    % Desired control
    ud = us(:,k);
    xd = xs(:,k);
    
    % TVLQR
    [~, A, B] = S.f(xd, ud, S);
    [~,K,~] = idare(A,B,Ql,Rl);
    u = -K*(xe-xd) + ud;
    
    % Propogate true state and time
    xt = S.f(xts(:,k), u, S) + sqrt(S.O)*randn(3,1);
    t = k*dt;
    
    % Propogate state estimation
    [xe,P] = ekf_predict(xe, P, u, S);                                        % predict
    [y, idx] = S.H(xt, S);
    E = diag(repmat(S.E, length(y)/2, 1));
    z = S.H(xt, S) + sqrt(E)*randn(length(y),1);                              % generate measurement
    [xe,P] = ekf_correct(xe, P, z, idx, S);                                   % correct
    
    % Save everything
    xts(:,k+1) = xt;
    ts(k+1) = t;
    xes(:,k+1) = xe;
    Ps(:,:,k+1) = P;
    ds(:,k+1) = xe - xts(:,k+1);                                              % actual estimate error
    ds(:,k+1) = fix_state(ds(:,k+1));
    
    comptime = toc;
    if (simtime)
        if (dt > comptime)
            pause(dt-comptime);
        end
    end
    tic;
    
    % Plot true and estimated states
    plot(xts(1,1:k), xts(2,1:k), '-g','LineWidth',2)                         % Plot true states
    plot(xes(1,1:k), xes(2,1:k), '-b','LineWidth',2)                          % Plot est states
    plotcov2(xes(1:2,k), 1.96^2*Ps(1:2,1:2,k));                               % Plot covariances
    plotcircle(xes(1,k),xes(2,k),S.limit);
    drawnow;
        
end

% Plot true and estimated states
plot(xts(1,1:k+1), xts(2,1:k+1), '-g','LineWidth',2)                         % Plot true states
plot(xes(1,1:k+1), xes(2,1:k+1), '-b','LineWidth',2)                          % Plot est states
plotcov2(xes(1:2,k+1), 1.96^2*Ps(1:2,1:2,k+1));                               % Plot covariances
plotcircle(xes(1,k+1),xes(2,k+1),S.limit);
drawnow;

%% Print results

err = xts(:,end)-S.xd;
disp(['Fin err: ', num2str(norm(err(1:2)))]);
disp(['Fin  det(P): ', num2str(det(Ps(1:2,1:2,end)))]);
disp(['Mean det(P): ', num2str(det(mean(Ps(1:2,1:2,:),3)))]);