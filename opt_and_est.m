%% Trajectory Optimization
% Perform trajectory optimization on unicycle dynamical system
% Perform states estimation along the planned trajectory

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('dysys');
addpath('opt');
addpath('est');
addpath('utility');
addpath('meas');

% initialize rng
rng(10212)

%% Timing

dt = .1;
N = 50;
T = dt*N;   
S.dt = dt;
S.h = dt;
S.tf = T;
S.N = N;

%% Beacons

% two beacons at (-2,2) and (2,2) : system is observable (two or more)
S.pbs = [-2, 2;
         2, 2];                                                             % beacon positions

%% Dynamical Model

S.n = 3;                                                                    % state dimension
S.f = @uni_f;                                                               % mobile-robot dynamics

%% Measurement Model

S.H = @br_h;                                                                % bearing-reange sensing

%% Noise Model

S.O = diag([.001 .001 .0001]);                                              % dynamics noise
S.E = [0.01; 0.001];

%% Trajectory Optimization Parameters

% cost function parameters
S.Q = diag([0, 0, 0]);
S.R = diag([1, 5]);
S.Qf = diag([5, 5, 1]);

% loss functions
S.L = @loss;
S.Lf = @final_loss;

%% Initialize Everything for Optimization

% initial covariance
P = diag([0.01 0.01 .001]);                                                 % initial covariance

% initial state
x0 = [0; 0; 0];                                                             % initial planned state
xt = x0;                                                                    % initial true state
xe = xt + sqrt(P)*randn(S.n, 1);                                            % initial estimated state

% initial trajectories
us = zeros(2,S.N);                                                          % planned control
xs = traj(x0, us, S);                                                       % planned states

% initialize matrices to track everything
xts = zeros(S.n, N+1);                                                      % true states
xes = zeros(S.n, N+1);                                                      % estimated states
Ps = zeros(S.n, S.n, N+1);                                                  % estimated covariances
ts = zeros(N+1,1);                                                          % times
ds = zeros(S.n, N+1);                                                       % errors

% first values
xts(:, 1) = xt;
xes(:, 1) = xe;
Ps(:, :, 1) = P;
ts(1) = 0;
ds(:,1) = xe - xt;

% goals
S.xd = [3;3;0];                                                             % desired goal state
S.ud = [0;0];                                                               % desired control

%% Optimize trajectory

[xs, us, blah, ~,~] = dircol(xs, us, S, @plottraj);

%% Simulate

for k=1:N
  
  % Set control
  u = us(:,k);
  
  % Propogate true state and time
  xt = S.f(xts(:,k), u, S) + sqrt(S.O)*randn(3,1);
  t = k*dt;

  % Propogate state estimation 
  [xe,P] = ekf_predict(xe, P, u, S);                  % predict
  [y, idx] = S.H(xt, S);
  E = diag(repmat(S.E, length(y)/2, 1));
  z = S.H(xt, S) + sqrt(E)*randn(length(y),1);          % generate measurement  
  [xe,P] = ekf_correct(xe, P, z, idx, S);                  % correct
  
  % Save everything
  xts(:,k+1) = xt;
  ts(k+1) = t;
  xes(:,k+1) = xe;
  Ps(:,:,k+1) = P;
  ds(:,k+1) = xe - xts(:,k+1);  % actual estimate error
  ds(:,k+1) = fix_state(ds(:,k+1));
    
end

%% Plot results

% Show trajectory and state estimation
figure('units','normalized','outerposition',[0 0 1 1]);
hold on;
xlabel('p_x');
ylabel('p_y');
axis equal;
axis xy;
plot(S.pbs(1,:), S.pbs(2,:), '*r');                                         % Plot beacons
plot(xs(1,1:N), xs(2,1:N), '-r','LineWidth',3)                              % Plot planned states

for k=1:N
  
  tic;
  plot(xts(1,1:k), xts(2,1:k), '--g','LineWidth',3)                         % Plot true states
  plot(xes(1,1:k), xes(2,1:k), '-b','LineWidth',3)                          % Plot est states
  plotcov2(xes(1:2,k), 1.96^2*Ps(1:2,1:2,k));                               % Plot covariances
  comp_tim = toc;
  pause(dt-comp_tim)
  drawnow;
  
end

legend('true', 'estimated');