function [x, varargout] = uni_f(x, u, S)
% dynamical model of the unicycle
c = cos(x(3));
s = sin(x(3));

x = [x(1) + c*u(1);
     x(2) + s*u(1);
     x(3) + u(2)];

x = fix_state(x, S);

if nargout > 1
  % F-matrix
  varargout{1} = [1, 0, -s*u(1);
                  0, 1, c*u(1); 
                  0 0 1];
end              
if nargout > 2
  varargout{2} = [c, 0;
                  s, 0; 
                  0, 1]; 
end
end
