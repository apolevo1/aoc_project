%% Trajectory Optimization
% Perform trajectory optimization on unicycle dynamical system

%% Clean

% clear everything
clear all;
close all;
clc;

% add paths
addpath('dysys');
addpath('opt');
addpath('utility');


% initialize rng
rng(10212)

%% Set up timing

dt = .1;
N = 50;
T = dt*N;
S.dt = dt;
S.tf = T;
S.N = N;
S.h = S.tf/S.N;

%% Set up cost/loss

% cost function parameters
S.Q = diag([0, 0, 0]);
S.R = diag([1, 5]);
S.Qf = diag([5, 5, 1]);

% loss functions
S.L = @loss;
S.Lf = @final_loss;

%% Set up dynamics

S.f = @uni_f;

%% Initialize Everything

% initial state
x0 = [0; 0; 0];

% initial control and state trajectories
us = zeros(2,S.N);
xs = traj(x0, us, S);

% desired state and control
S.xd = [5;5;0];
S.ud = [0;0];

% optimize trajector
[xs, us, blah, ~,~] = dircol(xs, us, S, @plottraj);

%% Plot result
plottraj(xs, us, S)