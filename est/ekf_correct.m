function [x,P] = ekf_correct(x, P, z, idxz, S)

% Get measurements
[y, idxy, H] = S.H(x, S);

% Match measurements
[~, iz, iy] = intersect(idxz, idxy);

iy = [iy*2-1, iy*2]';
iy = iy(:);
iz = [iz*2-1, iz*2]';
iz = iz(:);
z = z(iz);
y = y(iy);
H = H(iy,:);

if isempty(y)
    return;
end

% Get Noise
E = diag(repmat(S.E, length(y)/2, 1));

% Get Kalman Gain
P = P - P*H'*inv(H*P*H' + E)*H*P;
K = P*H'*inv(E);

% Correct State
e = z - y;
e = fix_meas(e, S);  % fix any [-pi,pi] issues
x = x + K*e;
