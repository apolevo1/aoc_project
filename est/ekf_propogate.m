function [Ps] = ekf_propogate(us, xs, S)

S.limit = S.limit - 2*sqrt(.001^2+.001^2);
P = S.P;
Ps = zeros(3,3,length(us)+1);
Ps(:, :, 1) = P;
for k=1:length(us)
  
  % Predict 
  [~, F] = S.f(xs(:,k), us(:,k), S);
  P = F*P*F' + S.O;
  
  % Correct
  [~, ~, H] = S.H(xs(:,k), S);
  if ~isempty(H)
    E = diag(repmat(S.E, size(H,1)/2, 1));
    P = P - P*H'*inv(H*P*H' + E)*H*P;
  end
  
  % Save
  Ps(:,:,k+1) = P;

end
S.limit = S.limit + 2*sqrt(.001^2+.001^2);
end

